# Template pour un module du backend CarbuRe

Ce dossier contient une suggestion de structure standardisée pour les modules du backend django de CarbuRe.

## Dossier `api`

Liste les différents endpoints de l'API de ce module.

Généralement, on essaie de créer un fichier par endpoint et de faire matcher le chemin de l'endpoint avec le chemin du fichier dans ce dossier `api`. On évite d'utiliser des caractères spéciaux dans les noms des fichiers et dans ceux des urls, et si une url utilise `-`, le fichier/dossier correspondant devra utiliser `_` à la place.

Plusieurs endpoints peuvent être groupés dans un même sous-dossier, auquel cas on pourra y créer un fichier `__init__.py` qui crée les `urlpatterns` demandés par django.

Pour plus de cohérence, les endpoints doivent suivre une structure similaire à celle décrite dans les fichiers `api/admin/get_some_endpoint.py` et `api/operator/post_some_endpoint.py`.

## Dossier `factories`

Les `factories` sont des générateurs aléatoires d'instances de models. Ils sont particulièrement utiles pour les tests automatisés, et permettent de générer un set de données cohérentes rapidement afin de ne pas avoir à tout créer manuellement à chaque fois.

## Dossier `models`

Liste les différents models pour gérer les tables de la base de données.
De préférence, créer un fichier par model, et ensuite exporter ces models depuis `models/__init__.py`.

## Dossier `parsers`

Les `parsers` sont des outils qui permettent de parser le contenu de requêtes HTTP. Ils sont utiles pour valider rapidement les données envoyées à un endpoint et les transformer en données utilisables par le reste de la logique. Le plus souvent, ils pourront être basés sur les `forms.Form` de django qui facilient grandement ce genre de logique.

Si un parser n'est utile qu'à un seul endpoint, il peut être défini directement à l'intérieur du fichier de l'endpoint. S'il commence à être réutilisé par plusieurs endpoints, il est plus intéressant de le déplacer dans le dossier `parsers`.

## Dossier `repositories`

Les `repositories` sont des outils qui permettent d'abstraire les appels à la base de donnée afin de fournir une base saine et standardisée pour lire et écrire dans la DB.

De même que pour les `parsers`, placer du code dans ce dossier n'est utile que lorsque la logique doit être réutilisée à plusieurs endroits.

## Dossier `scripts`

Certains modules ont besoin que des actions manuelles soient effectuées ponctuellement ou régulièrement. Dans ces cas là, un fichier devrait être créé dans le dossier `scripts` de ce module, qui contiendra la logique nécessaire pour réaliser cette opération.

## Dossier `serializers`

Les `serializers` sont des outils permettant de convertir les données brutes des models django en un format qui peut être exporté, généralement dans une réponse HTTP, mais aussi vers un fichier excel ou équivalent.

## Dossier `services`

Les `services` sont des bouts de logique réutilisable. Ils ne sont pas aussi spécialisés que les dossiers précédents en termes techniques, c'est à dire qu'ils n'ont pas de rôle particulier dans le déroulement d'un endpoint. Leur but est simplement d'abstraire un process qui peut être utile à plusieurs endpoints, mais qui ne rentre pas dans les cases très spécifiques des dossiers décrits précédemment.

## À propos des tests

Les fichiers de tests automatisés doivent être placés au plus près des fichiers qu'ils testent, donc dans le même dossier que ces fichiers.

Les types de tests qui nous intéressent le plus sont les tests d'intégration car ils forment un juste milieu entre la minutie des tests unitaires et le global des tests end-to-end.

Dans notre cas, on se concentrera particulièrement sur la simulation de requêtes HTTP vers les endpoints API que l'on veut vérifier. Les seuls autres composants qui méritent des tests poussés sont les `services` car ils peuvent contenir des logiques métiers assez compliquées qui peuvent avoir un impact significatif sur le comportement du serveur.

## Questions et idées

- Créer un dossier `errors` pour lister des types d'erreur réutilisables ?
- Ou alors un dossier `constants` qui contient tous les types de données statiques ?
- Où stocker les helpers plus génériques comme les décorateurs ?
- Faudrait-il suffixer tous les noms de fichiers pour contenir leur catégorie ? Par exemple `lot_model.py` au lieu de juste `lot.py`, afin d'éviter toute confusion sur les noms de fichier ?




