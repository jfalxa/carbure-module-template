# Dans ce fichier on définit un module django, afin de pouvoir le lister dans les apps de settings.py

from django.apps import AppConfig


class SomeModuleConfig(AppConfig):
    name = "some_module"
