from django.test import TestCase
from django.urls import reverse
from core.tests_utils import setup_current_user
from core.models import Entity
from some_module.factories import SomeModelFactory


class SomeModuleOperatorTest(TestCase):
    def setUp(self):
        self.operator = Entity.objects.create(
            name="Operator Test",
            entity_type=Entity.Operator,
        )

        self.user = setup_current_user(
            self,
            "tester@carbure.local",
            "Tester",
            "mdp",
            [(self.operator, "RW")],
        )

        self.test_models = SomeModelFactory.create_batch(100, owner=self.operator)

    def test_some_endpoint(self):
        response = self.client.post(
            reverse("some-module-operator-post-some"),
            {"entity_id": self.operator.id, "name": "test-name"},
        )

        self.assertEqual(response.status_code, 200)
