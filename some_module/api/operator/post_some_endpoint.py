import traceback
from django import forms
from django.db import transaction
from django.http import HttpRequest
from django.views.decorators.http import require_POST
from core.common import ErrorResponse, SuccessResponse
from core.decorators import check_admin_rights
from core.models import Entity, ExternalAdminRights
from some_module.parsers import SomeForm
from some_module.serializers import SomeSerializer
from some_module.repositories import SomeRepository


class PostSomeForm(forms.Form):
    name = forms.CharField(max_length=128)


class PostSomeError:
    MALFORMED_PARAMS = "MALFORMED_PARAMS"
    MODEL_CREATION_FAILED = "MODEL_CREATION_FAILED"


# généralement, un endpoint admin doit suivre la structure suivante
@require_POST
@check_admin_rights(allow_external=[ExternalAdminRights.ELEC])
def post_some(request: HttpRequest, entity: Entity):
    # 1. parser la data de la requêtre HTTP
    some_form = PostSomeForm(request.POST)

    # 2. valider la data et détecter des erreurs
    if not some_form.is_valid():
        return ErrorResponse(400, PostSomeError.MALFORMED_PARAMS, some_form.errors)

    name = some_form.cleaned_data["name"]

    try:
        # 3. dans le cas d'un POST, écrire des données dans la DB
        # généralement, si un endpoint doit modifier plusieurs tables de la DB
        # s'assurer que toutes ces opérations sont groupées dans une seule transaction
        # afin d'éviter de corrompre les données si une erreur se produit
        with transaction.atomic():
            some_model = SomeRepository().create_some_model(name=name, owner=entity)

        # 4. sérialiser les résultats
        serialized = SomeSerializer(some_model)

        # 5. renvoyer la réponse HTTP avec la data voulue
        return SuccessResponse(serialized.data)
    except:
        traceback.print_exc()
        return ErrorResponse(404, PostSomeError.MODEL_CREATION_FAILED)
