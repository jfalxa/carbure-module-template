# la liste des endpoints api pour la section `operator` de ce module

from django.urls import path
from .post_some_endpoint import post_some


urlpatterns = [
    path("some", post_some, name="some-module-operator-post-some"),
]
