# la liste des endpoints api pour la section `admin` de ce module

from django.urls import path
from .get_some_endpoint import get_some


urlpatterns = [
    path("some", get_some, name="some-module-admin-get-some"),
]
