import traceback
from django import forms
from django.http import HttpRequest
from django.views.decorators.http import require_GET
from core.common import ErrorResponse, SuccessResponse
from core.decorators import check_admin_rights
from core.models import Entity, ExternalAdminRights
from some_module.serializers import SomeSerializer
from some_module.repositories import SomeRepository


class GetSomeForm(forms.Form):
    some_id = forms.CharField(max_length=64)


class GetSomeError:
    MALFORMED_PARAMS = "MALFORMED_PARAMS"
    MODEL_NOT_FOUND = "MODEL_NOT_FOUND"


# généralement, un endpoint admin doit suivre la structure suivante
@require_GET
@check_admin_rights(allow_external=[ExternalAdminRights.ELEC])
def get_some(request: HttpRequest, entity: Entity):
    # 1. parser la data de la requêtre HTTP
    some_form = GetSomeForm(request.GET)

    # 2. valider la data et détecter des erreurs
    if not some_form.is_valid():
        return ErrorResponse(400, GetSomeError.MALFORMED_PARAMS, some_form.errors)

    model_id = some_form.cleaned_data["some_id"]

    try:
        # 3. dans le cas d'un GET, lire de la donnée de la DB
        # pour maximiser les performances, s'assurer qu'un minimum de requête est envoyé à la DB
        # et que les models obtenus contiennent TOUTES les informations dont le serializer a besoin
        some_model = SomeRepository().get_some_model(model_id, entity)

        # 4. serialiser les données obtenues
        serialized = SomeSerializer(some_model)

        # 5. renvoyer la réponse HTTP
        return SuccessResponse(serialized.data)
    except:
        traceback.print_exc()
        return ErrorResponse(404, GetSomeError.MODEL_NOT_FOUND)
