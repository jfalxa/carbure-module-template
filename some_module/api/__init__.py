# la liste combinée des endpoints api pour la section `admin` et `operator` de ce module

from django.urls import path, include

urlpatterns = [
    path("admin/", include("some_module.api.admin")),
    path("operator/", include("some_module.api.operator")),
]
