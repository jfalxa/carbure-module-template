from django.db import models


class SomeModel(models.Model):
    class Meta:
        db_table = "some_model"
        verbose_name = "Some Model"
        verbose_name_plural = "Some Models"

    PENDING = "PENDING"
    ACCEPTED = "ACCEPTED"
    REJECTED = "REJECTED"

    STATUSES = (
        (PENDING, "En attente"),
        (ACCEPTED, "Accepté"),
        (REJECTED, "Refusé"),
    )

    name = models.CharField(max_length=128)
    status = models.CharField(max_length=32, choices=STATUSES, default=PENDING)
    owner = models.ForeignKey("core.Entity", on_delete=models.CASCADE)
