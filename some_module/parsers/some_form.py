from django import forms


class SomeForm(forms.Form):
    status = forms.CharField()
    name = forms.CharField(max_length=128)
