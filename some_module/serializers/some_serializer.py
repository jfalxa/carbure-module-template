from rest_framework import serializers
from core.serializers import EntityPreviewSerializer
from some_module.models import SomeModel


class SomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SomeModel
        fields = [
            "id",
            "status",
            "owner",
        ]

    owner = EntityPreviewSerializer()
