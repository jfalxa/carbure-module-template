import factory

from some_module.models import SomeModel
from core.models import Entity


class SomeModelFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SomeModel

    name = factory.Faker("company")
    status = factory.Iterator(SomeModel.STATUSES, getter=lambda choice: choice[0])
    owner = factory.Iterator(Entity.objects.all())
