import argparse


def some_script(param: int):
    return param * 10


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run some script")
    parser.add_argument("--param", dest="param", action="store", help="Some param")
    args = parser.parse_args()
    some_script(args.param)
