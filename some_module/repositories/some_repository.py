from core.models import Entity
from some_module.models import SomeModel


class SomeRepository:
    def get_some_model(id: int, owner: Entity):
        return SomeModel.objects.get(pk=id, owner=owner)

    def create_some_model(name: str, owner: Entity):
        return SomeModel.objects.create(
            name=name,
            owner=owner,
            status=SomeModel.PENDING,
        )
