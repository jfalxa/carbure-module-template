# Ici on définit les différents éléments qui seront affichés dans la page /admin de django

from django.contrib import admin


class SomeModelAdmin(admin.ModelAdmin):
    list_display = ["name", "status", "owner"]
    search_fields = ["name"]
    list_filter = ["status", "owner"]
