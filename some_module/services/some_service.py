def some_service():
    # un servie est un bout de logique réutilisable
    # il peut faire n'importe quoi, et n'est pas aussi techniquement spécialisé que les autres dossiers du module
    # généralement, un service est utile lorsque certains processus doivent être répétés dans différents endpoints
    return 0
